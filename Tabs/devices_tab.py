from PyQt5 import QtGui, QtWidgets
from Dialog.output_device_chooser import OutputDeviceChooser
from Tabs.pdf_tab import displayed_screens

is_X11_platform = True

try:
    from X11.xcb_proxy import XcbProxy
except ImportError:
    is_X11_platform = False

class Devices_Tab(QtWidgets.QWidget):

    def __init__(self, root):
        super().__init__()

        self.root = root

        self.layout = QtWidgets.QHBoxLayout(self.root.tabWidget)
        self.setObjectName("devices_tab")
        self.setLayout(self.layout)
        self.root.tabWidget.addTab(self, "")

        self.odc = OutputDeviceChooser(self.root, False)
        self.odc.setFixedWidth(350)
        self.layout.addWidget(self.odc)

        self.addDevices()

    def updateDevices(self):
        for i in range(self.buttonsLayout.count()):
            self.buttonsLayout.itemAt(i).itemAt(0).widget().hide()
            self.buttonsLayout.itemAt(i).itemAt(1).widget().hide()

        self.layout.removeWidget(self.odc)
        self.odc.hide()
        self.odc = OutputDeviceChooser(self.root, False)
        self.odc.setFixedWidth(350)
        self.layout.addWidget(self.odc)

        self.layout.removeItem(self.buttonsLayout)
        self.addDevices()

    def setProjectedOutputIcon(self):
        for key, value in displayed_screens.items():
            deviceControler = QtGui.QPixmap(r'./icons/main_device_connected.png') \
                if is_X11_platform else QtGui.QPixmap(r'icons\main_device_connected.png')

            if self.buttonsLayout.itemAt(key) != None:
                self.buttonsLayout.itemAt(key).itemAt(0).widget().setPixmap(deviceControler)

    def addDevices(self):

        self.buttonsLayout = QtWidgets.QVBoxLayout(self)
        if is_X11_platform:
            XcbProxy.establishConnection()
        output = XcbProxy.getOutputs() if is_X11_platform else QtWidgets.QApplication.screens()

        for i in output:
            self.device_layout = QtWidgets.QHBoxLayout(self)
            deviceControler = QtGui.QPixmap(r'./icons/device_connected.png') \
                              if is_X11_platform else QtGui.QPixmap(r'icons\device_connected.png')

            if is_X11_platform and i['connection'] == 1:
                deviceControler = QtGui.QPixmap(r'./icons/device_disconected.png')

            icon_label = QtWidgets.QLabel()
            font = QtGui.QFont('Arial', 16)
            text_label = QtWidgets.QLabel(i['name'] + str(" ") + str("(") + str(i['width_pix']) + str(" x ") + str(i['height_pix']) + str(")")) \
                         if is_X11_platform else QtWidgets.QLabel(i.name())
            text_label.setFont(font)

            self.device_layout.addStretch()
            self.device_layout.insertWidget(0, text_label)
            self.device_layout.insertWidget(0, icon_label)

            icon_label.setPixmap(deviceControler)
            self.buttonsLayout.addLayout(self.device_layout)

        self.layout.addLayout(self.buttonsLayout)


