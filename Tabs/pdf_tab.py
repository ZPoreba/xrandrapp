from PyQt5 import QtCore, QtGui, QtWidgets
from projected_window import Projected_Window
from Dialog.output_device_chooser import OutputDeviceChooser

is_X11_platform = True

try:
    import popplerqt5
    from X11.xcb_proxy import XcbProxy
except ImportError:
    import fitz
    is_X11_platform = False

displayed_screens = {}

class Pdf_Tab(QtWidgets.QScrollArea):
    def __init__(self, root):
        super().__init__()
        self.root = root

        self.layout = QtWidgets.QVBoxLayout(self.root.tabWidget)
        self.setObjectName("pdf_tab")
        self.setLayout(self.layout)
        self.root.tabWidget.addTab(self, "")

        #create label for pixmap
        self.scroll = QtWidgets.QScrollArea()
        self.scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        #create pixmap with pdf, set list of files, set count for direct buttons
        self.pdf_pixmap = None
        self.file_list = None
        self.count = 0
        self.file = None
        self.projectedWindowNumber = -1
        self.projectedWindow = None


    def outputDialogWindow(self):
        dWindow = QtWidgets.QDialog()

        dWindowLayout = QtWidgets.QVBoxLayout(dWindow)
        outputChooser = OutputDeviceChooser(self.root, True)
        dWindowLayout.addWidget(outputChooser)

        dWindow.setWindowTitle("Choose output port:")
        dWindow.setWindowModality(QtCore.Qt.ApplicationModal)
        dWindow.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)
        dWindow.exec_()

        return outputChooser.getNumber() if outputChooser.getNumber() != None else -2


    def resizeEvent(self, event):
        if self.pdf_pixmap != None:
            pixmap = self.pdf_pixmap.scaledToWidth(self.width())
            self.label.setPixmap(pixmap)
            self.label.resize(pixmap.width(), pixmap.height())
            self.scroll.setWidget(self.label)
            self.layout.addWidget(self.scroll)

    def showDialog(self, monitors):
        if len(self.root.App.screens()) > 1:
            if len(monitors) > 1:
                self.projectedWindowNumber = self.outputDialogWindow()
                if self.projectedWindowNumber != -1:
                    self.projectedWindow = Projected_Window(self.root, self.projectedWindowNumber)
            else:
                self.projectedWindowNumber = 1
                self.projectedWindow = Projected_Window(self.root, self.projectedWindowNumber)

            if self.projectedWindowNumber not in displayed_screens:
                displayed_screens.update({self.projectedWindowNumber: self.projectedWindow})

    def addProjectedWindow(self, number):
        self.projectedWindowNumber = number
        self.projectedWindow = Projected_Window(self.root, self.projectedWindowNumber)
        if self.file != None:
            page = self.file.page(self.count) if is_X11_platform else self.file[self.count]
            self.projectedWindow.setProjection(page)

        if self.projectedWindowNumber not in displayed_screens.keys():
            displayed_screens.update({self.projectedWindowNumber: self.projectedWindow})

    def deleteProjectedWindow(self, number):
        if number in displayed_screens.keys():
            self.projectedWindow = displayed_screens[number]
            self.projectedWindowNumber = number
            self.projectedWindow.hide()
            if number in displayed_screens: del displayed_screens[number]

    def showPdf(self, pdf_path, number):
        self.label = QtWidgets.QLabel(self)
        self.label.setMinimumSize(1, 1)

        monitors = XcbProxy.getMonitors() if is_X11_platform else QtWidgets.QApplication.screens()
        self.showDialog(monitors)

        if is_X11_platform:
            self.pdf_path = pdf_path
            self.file = popplerqt5.Poppler.Document.load(pdf_path)
            dpi = XcbProxy.getOutputs()[0]['height'] * 0.0393700787 * 75
            self.pdf_pixmap = QtGui.QPixmap(self.file.page(number).renderToImage(dpi, dpi))
        else:
            self.file = fitz.open(pdf_path)
            mat = fitz.Matrix(3, 3)
            page = self.file[0]
            pix = page.getPixmap(matrix= mat, alpha=False)
            image = QtGui.QImage(pix.samples,
                                 pix.width, pix.height,
                                 pix.stride,
                                 QtGui.QImage.Format_RGB888)
            self.pdf_pixmap = QtGui.QPixmap.fromImage(image)

        if self.projectedWindow != None:
            if self.projectedWindow.label == None:
                if is_X11_platform:
                    self.projectedWindow.setProjection(self.file.page(number))
                else:
                    self.projectedWindow.setProjection(self.file[0])
            else:
                self.projectedWindow.changeProjection(self.file.page(number))

        pixmap = self.pdf_pixmap.scaledToWidth(self.width())

        self.label.setPixmap(pixmap)
        self.scroll.setWidget(self.label)
        self.layout.addWidget(self.scroll)


    def changeProjection(self, number):
        page = self.file.page(number) if is_X11_platform else self.file[number]

        if self.projectedWindow != None:
            for key, value in displayed_screens.items():
                self.projectedWindow = value
                self.projectedWindowNumber = key
                self.projectedWindow.changeProjection(page)


        if is_X11_platform:
            dpi = XcbProxy.getOutputs()[0]['height'] * 0.0393700787 * 75
            self.pdf_pixmap = QtGui.QPixmap(page.renderToImage(dpi, dpi))
        else:
            mat = fitz.Matrix(3, 3)
            pix = page.getPixmap(matrix=mat, alpha=False)
            image = QtGui.QImage(pix.samples,
                                 pix.width, pix.height,
                                 pix.stride,
                                 QtGui.QImage.Format_RGB888)
            self.pdf_pixmap = QtGui.QPixmap.fromImage(image)

        pixmap = self.pdf_pixmap.scaledToWidth(self.width())
        self.label.setPixmap(pixmap)
        self.scroll.setWidget(self.label)
        self.layout.addWidget(self.scroll)


    def rightPage(self):
        numPages = self.file.numPages() if is_X11_platform else len(self.file)
        if self.file != None and  self.count < numPages-1  and self.count >= 0:
            self.count += 1
            self.changeProjection(self.count)


    def leftPage(self):
        numPages = self.file.numPages() if is_X11_platform else len(self.file)
        if self.file != None and self.count < numPages and self.count > 0:
            self.count -= 1
            self.changeProjection(self.count)
