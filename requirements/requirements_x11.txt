setuptools==39.0.1
PyQt5-sip==4.19.15
PyQt5==5.12.2
python-poppler-qt5==0.24.2
sip==4.19.8
cffi==1.12.3
xcffib==0.5.1