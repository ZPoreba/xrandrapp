#additional class !! NOT USED IN PROJECT !!!
import subprocess


class XrandrInfo():

    portState = []
    connectedDevices = []

    #return all device ports, connected and disconected
    @staticmethod
    def getAllPorts():

        proc = subprocess.Popen(["xrandr | grep -E 'connected|disconnected'"], stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()

        without_brackets = str(out)[2:-1]
        XrandrInfo.portState = without_brackets.split("\\n")[:-1]

        for counter, port in enumerate(XrandrInfo.portState):
            XrandrInfo.portState[counter] = port.split(" ")[:2]

        XrandrInfo.getNumberOfConnected()
        return XrandrInfo.portState

    #add number of all connected ports
    @staticmethod
    def getNumberOfConnected():
        proc = subprocess.Popen(["xrandr --listmonitors"], stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
        lines = out.decode('ascii').split("\n")

        for i in range(1, len(lines)-1):
            l = lines[i].split(" ")

            try:
                ind = XrandrInfo.portState.index([l[5], 'connected']) #we want to find only connected devices
                XrandrInfo.portState[ind].append(l[1])
            except ValueError:
                continue

        return XrandrInfo.portState

if __name__ == "__main__":
    print(XrandrInfo.getAllPorts())