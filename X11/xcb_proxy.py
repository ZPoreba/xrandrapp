from PyQt5.QtX11Extras import QX11Info
import xcffib.xproto
import xcffib.randr as randr
import sip

class XcbProxy():

    __monitors = []
    __conn = None
    __outputs = []
    __setup = None
    __root = None

    @staticmethod
    def establishConnection():
        conn = sip.unwrapinstance(QX11Info.connection())
        XcbProxy.__conn = xcffib.wrap(conn)
        XcbProxy.__setup = XcbProxy.__conn.get_setup()
        XcbProxy.__root = XcbProxy.__setup.roots[0]

        return XcbProxy.__conn

    @staticmethod
    def checkExtention(ext):
        extensions = set(
            i.name.to_string().lower()
            for i in XcbProxy.__conn.core.ListExtensions().reply().names
        )

        if ext in extensions:
            return True
        else:
            return False

    @staticmethod
    def __decode_name(name):
        name = [str(chr(i)) for i in name]
        return "".join(name)


    @staticmethod
    def getOutputs():
        randr_conn = XcbProxy.__conn(xcffib.randr.key)
        resources = randr_conn.GetScreenResources(XcbProxy.__root.root).reply()

        XcbProxy.__outputs = []

        monitor_id = [monitor.outputs.list[0] for monitor in XcbProxy.getMonitors()]
        monitor_info = [monitor_info for monitor_info in XcbProxy.getMonitors()]
        monitors = dict(zip(monitor_id, monitor_info))

        for i in resources.outputs:
            output_reply = randr_conn.GetOutputInfo(i, xcffib.CurrentTime).reply()

            width = monitors[i].width if i in monitors.keys() else 0
            height = monitors[i].height if i in monitors.keys() else 0

            output = {'name': XcbProxy.__decode_name(output_reply.name), 'connection': output_reply.connection,
                      'height_pix': height, 'width_pix': width, 'height': output_reply.mm_height, 'width': output_reply.mm_width}
            if output not in XcbProxy.__outputs: XcbProxy.__outputs.append(output)

        return XcbProxy.__outputs


    @staticmethod
    def getMonitors():
        randr_conn = XcbProxy.__conn(xcffib.randr.key)
        XcbProxy.__monitors = randr_conn.GetMonitors(XcbProxy.__root.root, True).reply().monitors

        return XcbProxy.__monitors

    @staticmethod
    def tmp_check_dict_and_methods(obj):
        object_methods = [method_name for method_name in dir(obj)
                          if callable(getattr(obj, method_name))]

        print(object_methods)
        print(obj.__dict__)

    @staticmethod
    def setOutput():
        XcbProxy.old_outputs_connection = [(output['name'], output['connection']) for output in XcbProxy.__outputs.copy()]
        XcbProxy.new_outputs_connection = [(output['name'], output['connection']) for output in XcbProxy.getOutputs()]

    @staticmethod
    def checkIfAdded():
        new_old = [i for i in XcbProxy.new_outputs_connection if i not in XcbProxy.old_outputs_connection]

        if len(new_old) > 0:
            return_value = [output for output in XcbProxy.getOutputs() if output['name'] == new_old[0][0]][0]
            if return_value['connection'] == 0:
                return XcbProxy.__outputs.index(return_value)
            else:
                return -1
        else:
            return -1

    @staticmethod
    def checkIfDeleted():
        new_old = [i for i in XcbProxy.old_outputs_connection if i not in XcbProxy.new_outputs_connection]

        if len(new_old) > 0:
            return_value = [output for output in XcbProxy.getOutputs() if output['name'] == new_old[0][0]][0]
            if return_value['connection'] == 1:
                return XcbProxy.__outputs.index(return_value)
            else:
                return -1
        else:
            return -1

    @staticmethod
    def checkIfDuplicated(screen_number):
        monitors_to_check = XcbProxy.getMonitors()

        if monitors_to_check[0].x == monitors_to_check[screen_number].x \
                and monitors_to_check[0].y == monitors_to_check[screen_number].y:
            return True
        else:
            return False

    @staticmethod
    def separate(screen_number):

        randr_conn = XcbProxy.__conn(xcffib.randr.key)

        primary_output_reply = randr_conn.GetOutputPrimary(XcbProxy.__root.root).reply()
        primaty_output_info = randr_conn.GetOutputInfo(primary_output_reply.output, xcffib.CurrentTime).reply()

        resources = randr_conn.GetScreenResources(XcbProxy.__root.root).reply()
        config_timestamp = resources.config_timestamp
        timestamp = resources.timestamp

        monitor = XcbProxy.getMonitors()[screen_number]
        monitor_width = monitor.width
        monitor_height = monitor.height
        monitor_x = monitor.x
        monitor_y = monitor.y

        for index, primary in enumerate(resources.crtcs):
            if primary == primaty_output_info.crtc:
                primary_index = index
                break

        if primary_index == screen_number:
            return False

        primary_monitor = XcbProxy.getMonitors()[primary_index]
        primary_monitor_width = primary_monitor.width
        primary_monitor_height = primary_monitor.height

        max_width = max([output['width'] for output in XcbProxy.getOutputs()])
        sum_height = sum([output['height'] for output in XcbProxy.getOutputs()])



        randr_conn.SetScreenSize(XcbProxy.__root.root, max(monitor_width, primary_monitor_width),
                                 monitor_height + primary_monitor_height, max_width, sum_height)

        crtcs_id = resources.crtcs[screen_number]
        crtcs_value = randr_conn.GetCrtcInfo(crtcs_id, xcffib.CurrentTime).reply()



        primary_id = primaty_output_info.crtc
        primary_value = randr_conn.GetCrtcInfo(primary_id, xcffib.CurrentTime).reply()

        randr_conn.SetCrtcConfig(primary_id,
                                 timestamp,
                                 config_timestamp,
                                 monitor_x, monitor_y + monitor_height, primary_value.mode, 1,
                                 primary_value.num_outputs, primary_value.outputs)

        randr_conn.SetCrtcConfig(crtcs_id,
                                 timestamp,
                                 config_timestamp,
                                 monitor_x, monitor_y, crtcs_value.mode, 1,
                                 crtcs_value.num_outputs, crtcs_value.outputs)

        return True