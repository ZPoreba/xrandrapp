# X11App 

## Informacje ogólne
Aplikacja desktopowa X11App jest załącznikiem do pracy licencjackiej "Aplikacja do wyświetlania prezentacji z adnotacjami". Umożliwia wyświetlanie prezentacji w formacie PDF z wcześniej załączonymi notatkami. Użytkownik dostarcza odpowiednio skonstruowany plik PDF, w którym połowa strony jest przeznaczona na notatki, natomiast druga połowa stanowi właściwy slajd prezentacji. Głównym celem aplikacji jest ukazanie komunikacji klienta, jakim jest aplikacja z serwerem zarządzającym dodatkowym urządzeniem wyświetlającym w X Window System.
	
## Technologie
Projekt jest napisany w języku Python i wykorzystuje takie technologie jak:
* biblioteka PyQt5
* biblioteka popplerqt5
* biblioteka xcffib
* biblioteka sip
	
## Instalacja pakietów
Aby uruchomić aplikację należy zainstalować pakiety znajdujące się w odpowiednich plikach z wymaganiami w zależności od systemu kompatybilnego lub nie z X Window System.

Instalacja pakietów dla systemu kompatybilnego:

```
$ cd requirements
$ pip install -r requirements_x11.txt
```

Instalacja pakietów dla systemu niekompatybilnego:

```
$ cd requirements
$ pip install -r requirements_no_x11.txt
```

## Uruchomienie aplikacji

Aby uruchomić aplikację:

```
python main_frame.py
```

## Testowanie działania aplikacji

Plik PDF o nazwie input_data.pdf umożliwiający testowanie aplikacji znajduje się w katalogu test_pdfs. Jest specjalnie przygotowanym plikiem umożliwiającym ukazanie funkcjonowania aplikacji. Dlatego po uruchomieniu aplikacji, podczas ładowania pliku PDF należy za pomocą okna dialogowego wybrać plik input_data.pdf.


