from PyQt5 import QtCore, QtWidgets, QtGui

is_X11_platform = True

try:
    from X11.xcb_proxy import XcbProxy
except ImportError:
    import fitz
    is_X11_platform = False

all_windows = {}

class Projected_Window(QtWidgets.QWidget):


    keyPressed = QtCore.pyqtSignal(int)


    def __init__(self, root, number):
        super().__init__()
        self.MainWindow = root.MainWindow
        self.root = root
        self.label = None

        self.setAutoFillBackground(False)

        QtCore.QMetaObject.connectSlotsByName(self)
        root.MainWindow.setWindowToClose(self)

        self.setScreen(number)

    def keyPressEvent(self, event):
        super(Projected_Window, self).keyPressEvent(event)
        self.MainWindow.keyPressed.emit(event.key())


    def setScreen(self, number):
        self.number = number

        size_info = XcbProxy.getMonitors()[number] if is_X11_platform else QtWidgets.QApplication.screens()[number].size()
        width = size_info.width if is_X11_platform else size_info.width()
        height = size_info.height if is_X11_platform else size_info.height()
        self.resize(width, height)

        pDesktop = QtWidgets.QApplication.desktop()
        rectScreen0 = pDesktop.screenGeometry(number)
        self.move(rectScreen0.left(), rectScreen0.top())


    def changeProjection(self, page):
        if is_X11_platform:
            dpi = XcbProxy.getOutputs()[self.number]['height'] * 0.0393700787 * 75
            pixmap_tmp = QtGui.QPixmap(page.renderToImage(dpi, dpi))

            pixmap_cutted = pixmap_tmp.copy(0, 0, pixmap_tmp.width() / 2, pixmap_tmp.height())
            pixmap = pixmap_cutted.scaledToHeight(self.height())
        else:
            mat = fitz.Matrix(4, 4)
            pix = page.getPixmap(matrix=mat, alpha=False)
            image = QtGui.QImage(pix.samples,
                                 pix.width/2, pix.height,
                                 pix.stride,
                                 QtGui.QImage.Format_RGB888)
            pixmap = QtGui.QPixmap.fromImage(image).scaledToHeight(self.height())

        self.label.setPixmap(pixmap)

        self.label.setFixedSize(pixmap.width(), pixmap.height())
        self.label.move(self.width()/2 - self.label.width()/2, self.height()/2 - self.label.height()/2)


    def setProjection(self, page):

        if is_X11_platform:
            dpi = XcbProxy.getOutputs()[self.number]['height'] * 0.0393700787 * 75
            pixmap_tmp = QtGui.QPixmap(page.renderToImage(dpi, dpi))
            pixmap_cutted = pixmap_tmp.copy(0, 0, pixmap_tmp.width()/2, pixmap_tmp.height())
            pixmap = pixmap_cutted.scaledToHeight(self.height())
        else:
            mat = fitz.Matrix(4, 4)
            pix = page.getPixmap(matrix= mat, alpha=False)
            image = QtGui.QImage(pix.samples,
                                 pix.width/2, pix.height,
                                 pix.stride,
                                 QtGui.QImage.Format_RGB888)
            pixmap = QtGui.QPixmap.fromImage(image).scaledToHeight(self.height())

        self.label = QtWidgets.QLabel(self)
        self.label.setPixmap(pixmap)

        self.label.setFixedSize(pixmap.width(), pixmap.height())
        self.label.move(self.width()/2 - self.label.width()/2, self.height()/2 - self.label.height()/2)
        self.showFullScreen()