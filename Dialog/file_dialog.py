from PyQt5 import QtWidgets

class File_Dialog(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.fileName = None
        self.initUI()

    def initUI(self):
        self.openFileNameDialog()
        self.show()

    def openFileNameDialog(self):
        options = QtWidgets.QFileDialog.Options()

        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Choose PDF file", "",
                                              "PDF Files (*.pdf)", options=options)
        if fileName:
            self.fileName = fileName

    def getFileName(self):
        return self.fileName