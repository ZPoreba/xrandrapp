from PyQt5 import QtWidgets

is_X11_platform = True

try:
    from X11.xcb_proxy import XcbProxy
except ImportError:
    is_X11_platform = False

class OutputDeviceChooser(QtWidgets.QWidget):

    mainDeviceNumber = None

    def __init__(self, root, isInit):
        QtWidgets.QWidget.__init__(self)
        layout = QtWidgets.QGridLayout()
        self.setLayout(layout)
        self.listwidget = QtWidgets.QListWidget()
        self.root = root
        self.isInit = isInit

        for index, device in enumerate(QtWidgets.QApplication.screens()):
            self.listwidget.insertItem(index, device.name())

        layout.addWidget(self.listwidget)

        if self.isInit:
            button = QtWidgets.QPushButton("OK")
            button.clicked.connect(self.clicked)
            layout.addWidget(button)
        else:
            addButton = QtWidgets.QPushButton("+")
            addButton.clicked.connect(self.addClicked)
            layout.addWidget(addButton)

            removeButton = QtWidgets.QPushButton("-")
            removeButton.clicked.connect(self.removeClicked)
            layout.addWidget(removeButton)


    def clicked(self):
        self.mainDeviceNumber = self.listwidget.currentRow() if self.listwidget.currentRow() > -1 else 0

        if is_X11_platform:
            if XcbProxy.checkIfDuplicated(self.mainDeviceNumber):
                if XcbProxy.separate(self.mainDeviceNumber):
                    self.root.MainWindow.move(0, XcbProxy.getMonitors()[self.mainDeviceNumber].height)

        if not self.isInit:
            if self.root.pdf_tab.projectedWindow != None:
                self.root.pdf_tab.addProjectedWindow(self.mainDeviceNumber)
                self.root.pdf_tab.changeProjection(self.root.pdf_tab.count)
                self.root.pdf_tab.projectedWindow.show()

                self.root.devices_tab.updateDevices()
                self.root.devices_tab.setProjectedOutputIcon()
        else:
            self.parent().hide()

    def addClicked(self):
        self.mainDeviceNumber = self.listwidget.currentRow() if self.listwidget.currentRow() > -1 else 0

        if is_X11_platform:
            if XcbProxy.checkIfDuplicated(self.mainDeviceNumber) and self.mainDeviceNumber != 0:
                if XcbProxy.separate(self.mainDeviceNumber):
                    self.root.MainWindow.move(0, XcbProxy.getMonitors()[self.mainDeviceNumber].height)

        self.root.addMonitor(self.mainDeviceNumber)

    def removeClicked(self):
        self.mainDeviceNumber = self.listwidget.currentRow() if self.listwidget.currentRow() > -1 else 0
        self.root.deleteMonitor(self.mainDeviceNumber)

    def getNumber(self):
        return self.mainDeviceNumber