import time
from PyQt5 import QtCore

class ScreenNamesWorker(QtCore.QThread):

    numberWindowsList = []

    def __init__(self):
        QtCore.QThread.__init__(self)

    def setList(self, list):
        self.numberWindowsList = list

    def run(self):
        time.sleep(3)
        for i in self.numberWindowsList:
            i.hide()