import time
from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSignal
from X11.xcb_proxy import XcbProxy

class Worker(QtCore.QThread):

    addedMonitor = pyqtSignal(int, name='addedMonitor')
    deletedMonitor = pyqtSignal(int, name='deletedMonitor')

    def __init__(self):
        QtCore.QThread.__init__(self)

    def run(self):
        while True:
            XcbProxy.setOutput()
            added = XcbProxy.checkIfAdded()
            deleted = XcbProxy.checkIfDeleted()

            if added != -1:
                self.addedMonitor.emit(added)
            elif deleted != -1:
                self.deletedMonitor.emit(deleted)
            time.sleep(1)