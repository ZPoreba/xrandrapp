from PyQt5 import QtCore, QtGui, QtWidgets
from Dialog.file_dialog import File_Dialog
from Tabs.pdf_tab import Pdf_Tab, displayed_screens
from Tabs.devices_tab import Devices_Tab
import sys
from Workers.screen_names_worker import ScreenNamesWorker
import time

is_X11_platform = True

try:
    import popplerqt5
    from Workers.worker import Worker
    from X11.xcb_proxy import XcbProxy
except ImportError:
    is_X11_platform = False


class QMainWindow(QtWidgets.QMainWindow):

    keyPressed = QtCore.pyqtSignal(int)

    def __init__(self):
        super().__init__()
        self.windows = []

    def keyPressEvent(self, event):
        super(QMainWindow, self).keyPressEvent(event)
        self.keyPressed.emit(event.key())

    def setWidgetToClose(self, widget):
        self.widget = widget

    def setWindowToClose(self, window):
        self.windows.append(window)

    def closeEvent(self, event):
        for i in self.windows: i.hide()


class Ui_MainWindow(object):

    stylesheet = """
    QWidget{
        background-color: #4e4e4e;
        color: #ffffff; 
        font-family: Arial;
    }
    
    Pdf_Tab{
        background-color: #282828;
    }
    
    Projected_Window{
        background-color: #000000;
    }
    
    QScrollArea{
        background-color: #282828;
    }
    
    QLayout{
        background-color: #282828;
    }
    
    QTabBar::tab{ 
        color:  #09b7b7 
        } 
    
    """

    def setupUi(self, MainWindow, App):

        self.App = App
        self.MainWindow = MainWindow

        self.App.setStyleSheet(self.stylesheet)

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1200, 800)
        MainWindow.setAutoFillBackground(False)

        self.centralWidget()
        self.setInTheMiddle()

        self.layout = QtWidgets.QHBoxLayout(self.centralwidget)

        self.buttonSettings()
        self.tabSettings()

        self.retranslateUi()
        self.tabWidget.setCurrentIndex(0)

        if is_X11_platform:
            self.worker = Worker()
            self.worker.addedMonitor.connect(self.addMonitor)
            self.worker.deletedMonitor.connect(self.deleteMonitor)
            self.worker.start()

        QtCore.QMetaObject.connectSlotsByName(self.MainWindow)
        self.numberWorker = ScreenNamesWorker()

    def addMonitor(self, number):

        if self.pdf_tab.file != None and number not in displayed_screens:
            time.sleep(2)
            if is_X11_platform:
                if XcbProxy.separate(number):
                    self.MainWindow.move(0, XcbProxy.getMonitors()[number].height)

            self.pdf_tab.addProjectedWindow(number)
            self.devices_tab.updateDevices()
            self.devices_tab.setProjectedOutputIcon()
        else:
            self.devices_tab.updateDevices()
            self.devices_tab.setProjectedOutputIcon()

    def deleteMonitor(self, number):
        if self.pdf_tab.file != None:
            self.pdf_tab.deleteProjectedWindow(number)
            self.devices_tab.updateDevices()
            self.devices_tab.setProjectedOutputIcon()

    def setInTheMiddle(self):
        qtRectangle = self.MainWindow.frameGeometry()
        centerPoint = QtWidgets.QDesktopWidget().availableGeometry().center()
        qtRectangle.moveCenter(centerPoint)
        self.MainWindow.move(qtRectangle.topLeft())

    def centralWidget(self):
        self.centralwidget = QtWidgets.QWidget(self.MainWindow)
        self.centralwidget.setObjectName("CentralWidget")
        self.MainWindow.setCentralWidget(self.centralwidget)

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.MainWindow.setWindowTitle(_translate("App", "App"))
        self.pushButton_1.setText(_translate("App", "Open PDF"))
        self.pushButton_2.setText(_translate("App", "Exit"))
        self.pushButton_5.setText(_translate("App", "Device names"))

        self.tabWidget.setTabText(self.tabWidget.indexOf(self.pdf_tab), _translate("App", "PDF"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.devices_tab), _translate("App", "Devices"))


    def buttonSettings(self):
        self.buttonsLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.layout.addLayout(self.buttonsLayout)

        self.pushButton_1 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_1.setIconSize(QtCore.QSize(40, 40))
        self.pushButton_1.setIcon(QtGui.QIcon('./icons/file.png'))
        self.pushButton_1.setGeometry(QtCore.QRect(690, 540, 89, 25))
        self.pushButton_1.setObjectName("pushButton_1")
        self.pushButton_1.clicked.connect(self.openPDF)
        self.buttonsLayout.addWidget(self.pushButton_1)


        self.directButtons = QtWidgets.QHBoxLayout(self.centralwidget)

        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setIconSize(QtCore.QSize(40, 40))
        self.pushButton_3.setIcon(QtGui.QIcon('./icons/left.png'))
        self.pushButton_3.setGeometry(QtCore.QRect(690, 540, 89, 25))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(self.goLeft)

        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setIconSize(QtCore.QSize(40, 40))
        self.pushButton_4.setIcon(QtGui.QIcon('./icons/right.png'))
        self.pushButton_4.setGeometry(QtCore.QRect(690, 540, 89, 25))
        self.pushButton_4.setObjectName("pushButton_4")
        self.pushButton_4.clicked.connect(self.goRight)

        self.directButtons.addWidget(self.pushButton_3)
        self.directButtons.addWidget(self.pushButton_4)
        self.buttonsLayout.addLayout(self.directButtons)

        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setIconSize(QtCore.QSize(40, 40))
        self.pushButton_5.setIcon(QtGui.QIcon('./icons/devices.png'))
        self.pushButton_5.setGeometry(QtCore.QRect(690, 540, 89, 25))
        self.pushButton_5.setObjectName("pushButton_5")
        self.buttonsLayout.addWidget(self.pushButton_5)
        self.pushButton_5.clicked.connect(self.setScreensNames)

        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setIconSize(QtCore.QSize(40, 40))
        self.pushButton_2.setIcon(QtGui.QIcon('./icons/quit.png'))
        self.pushButton_2.setGeometry(QtCore.QRect(690, 540, 89, 25))
        self.pushButton_2.setObjectName("pushButton_2")
        self.buttonsLayout.addWidget(self.pushButton_2)
        self.pushButton_2.clicked.connect(self.exit)


    def tabSettings(self):
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(300, 10, 481, 521))
        self.tabWidget.setObjectName("tabWidget")

        self.layout.addWidget(self.tabWidget)

        self.pdf_tab = Pdf_Tab(self)
        self.devices_tab = Devices_Tab(self)

    def openPDF(self):
        fileDialog = File_Dialog(self.MainWindow)

        #check if file is picked and run display method
        if fileDialog.getFileName():
            self.pdf_tab.showPdf(fileDialog.getFileName(), 0)
            self.devices_tab.setProjectedOutputIcon()


    def goRight(self):
        if self.pdf_tab.file != None:
            self.pdf_tab.rightPage()

    def goLeft(self):
        if self.pdf_tab.file != None:
            self.pdf_tab.leftPage()

    def exit(self):
        try:
            self.pdf_tab.projectedWindow.hide()
            sys.exit()
        except:
            sys.exit()

    def changeDeviceState(self):
        self.devices_tab.addDevices()

    def setScreensNames(self):
        self.numberWindowsList = []

        for index, screen in enumerate(QtWidgets.QApplication.screens()):
            numberWindow = QtWidgets.QMainWindow(flags=QtCore.Qt.CustomizeWindowHint)
            numberWindow.setFixedSize(400, 250)
            self.numberWindowsList.append(numberWindow)

            pDesktop = QtWidgets.QApplication.desktop()
            rectScreen0 = pDesktop.screenGeometry(index)

            label = QtWidgets.QLabel(screen.name())
            font = QtGui.QFont('Arial', 64) if is_X11_platform else QtGui.QFont('Arial', 32)
            label.setFont(font)
            label.setAlignment(QtCore.Qt.AlignCenter)
            label.setFixedSize(numberWindow.width(), numberWindow.height())
            numberWindow.setCentralWidget(label)

            numberWindow.move(rectScreen0.left(), rectScreen0.top())
            numberWindow.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)
            numberWindow.show()

            self.numberWorker.setList(self.numberWindowsList)
            self.numberWorker.start()

    def keypressHandler(self, key):
        if key == QtCore.Qt.Key_Return or key == QtCore.Qt.Key_Space \
        or key == QtCore.Qt.Key_Right or key == QtCore.Qt.Key_Up:
            self.pushButton_4.clicked.emit()
        elif key == QtCore.Qt.Key_Left or key == QtCore.Qt.Key_Down:
            self.pushButton_3.clicked.emit()
        elif key == QtCore.Qt.Key_Escape:
            self.pushButton_2.clicked.emit()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QMainWindow()
    MainWindow.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)

    ui = Ui_MainWindow()
    ui.setupUi(MainWindow, app)

    MainWindow.keyPressed.connect(ui.keypressHandler)

    # set no focus on widgets and strong focus on MainWindow to catch keypress in MainWindow
    ui.tabWidget.setFocusPolicy(QtCore.Qt.NoFocus)

    for i in range(ui.buttonsLayout.count()):
        if ui.buttonsLayout.itemAt(i).widget() != None:
            ui.buttonsLayout.itemAt(i).widget().setFocusPolicy(QtCore.Qt.NoFocus)
        else:
            ui.buttonsLayout.itemAt(i).itemAt(0).widget().setFocusPolicy(QtCore.Qt.NoFocus)

    MainWindow.setFocusPolicy(QtCore.Qt.StrongFocus)

    MainWindow.show()
    sys.exit(app.exec_())